package dev.lachlan.daos;

import dev.lachlan.Log;
import org.postgresql.Driver;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

import static dev.lachlan.Credentials.POSTGRES_PASSWORD;
import static dev.lachlan.Credentials.POSTGRES_USER;
import static dev.lachlan.Credentials.POSTGRES_URL;

final class GetEmployeesList implements Log {
	static String getEmployeesList() throws SQLException {
		finerLogger.entering(GetEmployeesList.class.getName(), "getEmployeesList");

		String sql = 
				"SELECT first_name, last_name from employee;";

		DriverManager.registerDriver(new Driver());
		try (
				Connection connection = DriverManager.getConnection(POSTGRES_URL, POSTGRES_USER, POSTGRES_PASSWORD);
				Statement statement = connection.createStatement();
		) {
			ResultSet resultSet = statement.executeQuery(sql);
			StringBuilder stringBuilder = new StringBuilder();
			int count = 0;

			stringBuilder.append("[");

			while(true) {

				if(resultSet.next()) {
					stringBuilder.append("{\"firstName\":\"" + resultSet.getString("first_name") + "\",\"lastName\":\"" + resultSet.getString("last_name") + "\"}");
					count++;

					if(!resultSet.isLast()) {
						stringBuilder.append(",");
					}

				} else {
					break;
				}
				
			}

			stringBuilder.append("]");

			String employeesList = stringBuilder.toString();

			fineLogger.fine("Retrieved " + count + " records for employees list. Return String: " + employeesList);
			finerLogger.exiting(GetEmployeesList.class.getName(), "getEmployeesList");
			return employeesList;
		}
	}
}
