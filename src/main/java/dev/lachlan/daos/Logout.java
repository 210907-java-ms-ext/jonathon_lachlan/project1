package dev.lachlan.daos;

import dev.lachlan.Log;
import org.postgresql.Driver;
import java.sql.*;

import static dev.lachlan.Credentials.*;

public class Logout implements Log {
    static boolean logout(String authorizationToken) throws SQLException, DatabaseIntegrityException {
        finerLogger.entering(Logout.class.getName(), "logout");

        String sql =
                "DELETE FROM manager_authorization_session WHERE token = ?";

        DriverManager.registerDriver(new Driver());
        try(
                Connection connection = DriverManager.getConnection(POSTGRES_URL, POSTGRES_USER, POSTGRES_PASSWORD);
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ) {
            preparedStatement.setString(1, authorizationToken);

            int rowsAffected = preparedStatement.executeUpdate();

            if(rowsAffected == 1) {
                fineLogger.fine("Token removed: " + authorizationToken);
                finerLogger.exiting(Logout.class.getName(), "logout");
                return true;
            } else if(rowsAffected == 0){
                fineLogger.fine("Token not found: " + authorizationToken);
                finerLogger.exiting(Logout.class.getName(), "logout");
                return false;
            } else {
                throw new DatabaseIntegrityException("Unexpected rows affected count on delete: " + rowsAffected + " / authorizationCode: " + authorizationToken);
            }

        }
    }
}