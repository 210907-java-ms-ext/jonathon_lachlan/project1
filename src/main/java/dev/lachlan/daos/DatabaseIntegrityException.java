package dev.lachlan.daos;

public class DatabaseIntegrityException extends Exception {
    DatabaseIntegrityException(String message) {
        super(message);
    }
}