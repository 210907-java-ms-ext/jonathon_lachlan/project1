package dev.lachlan.daos;

import dev.lachlan.Log;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.PreparedStatement;

import static dev.lachlan.Credentials.POSTGRES_URL;
import static dev.lachlan.Credentials.POSTGRES_USER;
import static dev.lachlan.Credentials.POSTGRES_PASSWORD;

public class DenyReimbursementRequest implements Log {
    static String denyReimbursementRequest(int reimbursementRequestId, int managerId) throws SQLException, DatabaseIntegrityException {
        finerLogger.entering(DenyReimbursementRequest.class.getName(), "denyReimbursementRequest");

        String updateReimbursementRequestSql =
                "UPDATE reimbursement_request SET resolving_manager_id = ?, status='Denied' WHERE reimbursement_request_id = ?;";
        try (
                Connection connection = DriverManager.getConnection(POSTGRES_URL, POSTGRES_USER, POSTGRES_PASSWORD);
                PreparedStatement updateReimbursementRequestPreparedStatement = connection.prepareStatement(updateReimbursementRequestSql);
        ) {
            updateReimbursementRequestPreparedStatement.setInt(1, managerId);
            updateReimbursementRequestPreparedStatement.setInt(2, reimbursementRequestId);

            int rows = updateReimbursementRequestPreparedStatement.executeUpdate();

            if(rows > 1) {
                throw new DatabaseIntegrityException("More than one reimbursement_request was updated for a manager denial. reimbursement_request_id=" + reimbursementRequestId + " manager_id=" + managerId);
            } else if (rows == 0) {
                throw new DatabaseIntegrityException("No records were found for manager denial of reimbursement_request. reimbursement_request_id=\"" + reimbursementRequestId + "\" manager_id=\" + managerId");
//                throw new DatabaseOperationException("No records were found for manager denial of reimbursement_request. reimbursement_request_id=\"" + reimbursementRequestId + "\" manager_id=\" + managerId");
            } else {
                fineLogger.fine("Updated reimbursement_request with reimbursement_request_id:" + reimbursementRequestId + " - denied by manager with manager_id: " + managerId);

                String managerNameSql =
                        "SELECT first_name, last_name FROM manager WHERE manager_id = ?";
                try (
                        PreparedStatement managerNamePreparedStatement = connection.prepareStatement(managerNameSql);
                ) {
                    managerNamePreparedStatement.setInt(1, managerId);

                    ResultSet resultSet = managerNamePreparedStatement.executeQuery();

                    if (resultSet.next()) {

                        if(!resultSet.isLast())
                            throw new DatabaseIntegrityException("More than one manager found for manager_id: " + managerId);

                        String managerFirstName = resultSet.getString("first_name");
                        String managerLastName = resultSet.getString("last_name");
                        String managerName = managerFirstName + " " + managerLastName;

                        fineLogger.fine("Found manager name: " + managerName);
                        finerLogger.exiting(DenyReimbursementRequest.class.getName(), "denyReimbursementRequest");
                        return managerName;
                    } else {
                        throw new DatabaseIntegrityException("Manager not found for manager_id: " + managerId);
                    }

                }
            }

        }
    }
}
