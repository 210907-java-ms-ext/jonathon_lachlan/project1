package dev.lachlan.daos;

import dev.lachlan.Log;
import org.postgresql.Driver;

import java.sql.*;

import static dev.lachlan.Credentials.*;

class AddReimbursementRequest implements Log {

    static boolean addReimbursementRequest(int employeeId, String title, String amount) throws SQLException, DatabaseIntegrityException {
        finerLogger.entering(AddReimbursementRequest.class.getName(), "addReimbursementRequest");

        if(amount.matches("[ \t]*(\\$[ \t]*)?\\d*\\.?\\d{3,}0*[ \t]*") || amount.matches("[ \t]*(\\$[ \t]*)?-\\d*\\.?\\d{0,2}0*[ \t]*") || !amount.matches("[ \t]*(\\$[ \t]*)?\\d*\\.?\\d{0,2}0*[ \t]*"))
            return false;

        String[] amountSplit = amount
                .replaceAll("\\s*\\$? ?", "")
                .split("\\.");

        if(amountSplit.length == 0 || (amountSplit.length == 1 && amountSplit[0].equals("")))
            return false;

        int decimalLength = amountSplit.length == 2 ? amountSplit[AmountArrayEnum.decimal.index].length() : 0;
        int amountCentesimalInteger = Integer.parseInt(amountSplit[AmountArrayEnum.wholeNumber.index] + (amountSplit.length == 2 ? amountSplit[AmountArrayEnum.decimal.index] : "") + "0".repeat(2 - decimalLength));

        String sql =
                "INSERT INTO reimbursement_request (employee_id, title, amount_centesimal_integer, status) VALUES (?, ?, ?, 'Pending') RETURNING reimbursement_request_id";

        DriverManager.registerDriver(new Driver());
        try (
                Connection connection = DriverManager.getConnection(POSTGRES_URL, POSTGRES_USER, POSTGRES_PASSWORD);
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ) {
            preparedStatement.setInt(1, employeeId);
            preparedStatement.setString(2, title);
            preparedStatement.setInt(3, amountCentesimalInteger);
            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                fineLogger.fine("Added reimbursement request with reimbursement_request_id=" + resultSet.getInt("reimbursement_request_id") + " for employeeId=" + employeeId + " with title=" + title + " / amount=" + amount);
                finerLogger.exiting(AddReimbursementRequest.class.getName(), "addReimbursementRequest");
                return true;
            } else {
                throw new DatabaseIntegrityException("Record was not inserted when values are valid. employeeId=" + employeeId + " / title=" + title + " / amount=" + amount);
            }
        }
    }
}
