package dev.lachlan.daos;

import dev.lachlan.Log;
import org.postgresql.Driver;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;

import static dev.lachlan.Credentials.POSTGRES_URL;
import static dev.lachlan.Credentials.POSTGRES_USER;
import static dev.lachlan.Credentials.POSTGRES_PASSWORD;

public class Authorize implements Log {
    static boolean authorize(String authorizationToken) throws SQLException {
        finerLogger.entering(Authorize.class.getName(), "authorize");

        String sql =
                "SELECT FROM manager_authorization_session WHERE token = ?";

        DriverManager.registerDriver(new Driver());
        try(
                Connection connection = DriverManager.getConnection(POSTGRES_URL, POSTGRES_USER, POSTGRES_PASSWORD);
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ) {
            preparedStatement.setString(1, authorizationToken);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                fineLogger.fine("Token found: " + authorizationToken);
                finerLogger.exiting(Authorize.class.getName(), "authorize");
                return true;
            } else {
                fineLogger.fine("Token not found: " + authorizationToken);
                finerLogger.exiting(Authorize.class.getName(), "authorize");
                return false;
            }

        }
    }

    static int getManagerId(String authorizationToken) throws SQLException {
        finerLogger.entering(Authorize.class.getName(), "getManagerId");

        String sql =
                "SELECT manager_id FROM manager_authorization_session WHERE token = ?";

        DriverManager.registerDriver(new Driver());
        try(
                Connection connection = DriverManager.getConnection(POSTGRES_URL, POSTGRES_USER, POSTGRES_PASSWORD);
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ) {
            preparedStatement.setString(1, authorizationToken);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                fineLogger.fine("Token found: " + authorizationToken);
                finerLogger.exiting(Authorize.class.getName(), "getManagerId");
                return resultSet.getInt("manager_id");
            } else {
                fineLogger.fine("Token not found: " + authorizationToken);
                finerLogger.exiting(Authorize.class.getName(), "getManagerId");
                return -1;
            }

        }
    }

    static boolean authorizeEmployee(String authorizationToken) throws SQLException {
        finerLogger.entering(Authorize.class.getName(), "authorizeEmployee");

        String sql =
                "SELECT FROM manager_authorization_session WHERE token = ?";

        DriverManager.registerDriver(new Driver());
        try(
                Connection connection = DriverManager.getConnection(POSTGRES_URL, POSTGRES_USER, POSTGRES_PASSWORD);
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ) {
            preparedStatement.setString(1, authorizationToken);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                fineLogger.fine("Token found: " + authorizationToken);
                finerLogger.exiting(Authorize.class.getName(), "authorizeEmployee");
                return true;
            } else {
                fineLogger.fine("Token not found: " + authorizationToken);
                finerLogger.exiting(Authorize.class.getName(), "authorizeEmployee");
                return false;
            }

        }
    }


    static int getEmployeeId(String authorizationToken) throws SQLException {
        finerLogger.entering(Authorize.class.getName(), "getEmployeeId");

        String sql =
                "SELECT employee_id FROM employee_authorization_session WHERE token = ?";

        DriverManager.registerDriver(new Driver());
        try(
                Connection connection = DriverManager.getConnection(POSTGRES_URL, POSTGRES_USER, POSTGRES_PASSWORD);
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ) {
            preparedStatement.setString(1, authorizationToken);

            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                fineLogger.fine("Token found: " + authorizationToken);
                finerLogger.exiting(Authorize.class.getName(), "getEmployeeId");
                return resultSet.getInt("employee_id");
            } else {
                fineLogger.fine("Token not found: " + authorizationToken);
                finerLogger.exiting(Authorize.class.getName(), "getEmployeeId");
                return -1;
            }

        }
    }
}
