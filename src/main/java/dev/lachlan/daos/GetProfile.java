package dev.lachlan.daos;

import dev.lachlan.Log;
import org.postgresql.Driver;

import java.sql.*;

import static dev.lachlan.Credentials.POSTGRES_PASSWORD;
import static dev.lachlan.Credentials.POSTGRES_USER;
import static dev.lachlan.Credentials.POSTGRES_URL;

public class GetProfile implements Log {
    static String getProfile(int employeeId) throws SQLException {
        finerLogger.entering(GetEmployeesList.class.getName(), "getProfile");

        String sql =
                "SELECT first_name, last_name from employee WHERE employee_id = ?;";

        DriverManager.registerDriver(new Driver());
        try (
                Connection connection = DriverManager.getConnection(POSTGRES_URL, POSTGRES_USER, POSTGRES_PASSWORD);
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ) {
            preparedStatement.setInt(1, employeeId);

            ResultSet resultSet = preparedStatement.executeQuery();
            StringBuilder stringBuilder = new StringBuilder();
            int count = 0;

            stringBuilder.append("[");

            while(true) {

                if(resultSet.next()) {
                    stringBuilder.append("{\"firstName\":\"" + resultSet.getString("first_name") + "\",\"lastName\":\"" + resultSet.getString("last_name") + "\"}");
                    count++;

                    if(!resultSet.isLast()) {
                        stringBuilder.append(",");
                    }

                } else {
                    break;
                }

            }

            stringBuilder.append("]");

            String employeesList = stringBuilder.toString();

            fineLogger.fine("Retrieved " + count + " records for employees list. Return String: " + employeesList);
            finerLogger.exiting(GetEmployeesList.class.getName(), "getProfile");
            return employeesList;
        }
    }
}
