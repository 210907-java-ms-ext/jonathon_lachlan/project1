package dev.lachlan.daos;

public class Authentication {
    public boolean isUser;
    public String authorizationToken;

    Authentication() {
        super();
        isUser = false;
        authorizationToken = null;
    }

    Authentication(String authorizationToken) {
        super();
        this.isUser = true;
        this.authorizationToken = authorizationToken;
    }
}
