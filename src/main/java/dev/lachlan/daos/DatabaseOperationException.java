package dev.lachlan.daos;

public class DatabaseOperationException extends Exception {
    DatabaseOperationException(String message) {
        super(message);
    }
}
