package dev.lachlan.daos;

import dev.lachlan.Log;
import org.postgresql.Driver;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.util.Base64;

import static dev.lachlan.Credentials.POSTGRES_URL;
import static dev.lachlan.Credentials.POSTGRES_USER;
import static dev.lachlan.Credentials.POSTGRES_PASSWORD;

final class Authenticate implements Log {
    static Authentication authenticate(String email, byte[] password) throws SQLException, DatabaseIntegrityException, NoSuchAlgorithmException {
        finerLogger.entering(Authenticate.class.getName(), "authenticate");

        String managerLookupSql =
                "SELECT manager_id, password_sha256 FROM manager WHERE email = ?";

        DriverManager.registerDriver(new Driver());
        try(
                Connection connection = DriverManager.getConnection(POSTGRES_URL, POSTGRES_USER, POSTGRES_PASSWORD);
                PreparedStatement managerLookupPreparedStatement = connection.prepareStatement(managerLookupSql);
        ) {
            managerLookupPreparedStatement.setString(1, email);

            ResultSet resultSet = managerLookupPreparedStatement.executeQuery();

            if (!resultSet.next()) {
                fineLogger.fine("Record not found for email: " + email);
                finerLogger.exiting(Authenticate.class.getName(), "authenticate");
                return new Authentication();
            }

            if (!resultSet.isLast())
                throw new DatabaseIntegrityException("More than one manager record found for email address " + email);

            byte[] passwordSha256 = resultSet.getBytes("password_sha256");

            if(MessageDigest.isEqual(MessageDigest.getInstance("SHA-256").digest(password), passwordSha256)) {
                int managerId = resultSet.getInt("manager_id");
                KeyGenerator kg = KeyGenerator.getInstance("HmacSHA256");
                SecretKey sk = kg.generateKey();
                Base64.Encoder base64Encoder = Base64.getUrlEncoder();
                String token = base64Encoder.encodeToString(sk.getEncoded());
                String createManagerAuthorizationSessionSql =
                        "INSERT INTO manager_authorization_session (manager_id, token) VALUES (?, ?)";
                try(
                        PreparedStatement createManagerAuthorizationSessionPreparedStatement = connection.prepareStatement(createManagerAuthorizationSessionSql);
                ) {
                    createManagerAuthorizationSessionPreparedStatement.setInt(1, managerId);
                    createManagerAuthorizationSessionPreparedStatement.setString(2, token);
                    createManagerAuthorizationSessionPreparedStatement.execute();
                }
                fineLogger.fine("Created token for email: " + email + " / managerId: " + managerId + ". Token: " + token);
                finerLogger.exiting(Authenticate.class.getName(), "authenticate");
                return new Authentication(token);
            } else {
                fineLogger.fine("Password mismatch for email: " + email);
                finerLogger.exiting(Authenticate.class.getName(), "authenticate");
                return new Authentication();
            }

        }
    }

    static Authentication authenticateEmployee(String email, byte[] password) throws SQLException, DatabaseIntegrityException, NoSuchAlgorithmException {
        finerLogger.entering(Authenticate.class.getName(), "authenticateEmployee");

        String employeeLookupSql =
                "SELECT employee_id, password_sha256 FROM employee WHERE email = ?";

        DriverManager.registerDriver(new Driver());
        try(
                Connection connection = DriverManager.getConnection(POSTGRES_URL, POSTGRES_USER, POSTGRES_PASSWORD);
                PreparedStatement employeeLookupPreparedStatement = connection.prepareStatement(employeeLookupSql);
        ) {
            employeeLookupPreparedStatement.setString(1, email);

            ResultSet resultSet = employeeLookupPreparedStatement.executeQuery();

            if (!resultSet.next()) {
                fineLogger.fine("Record not found for email: " + email);
                finerLogger.exiting(Authenticate.class.getName(), "authenticateEmployee");
                return new Authentication();
            }

            if (!resultSet.isLast())
                throw new DatabaseIntegrityException("More than one employee record found for email address " + email);

            byte[] passwordSha256 = resultSet.getBytes("password_sha256");

            if(MessageDigest.isEqual(MessageDigest.getInstance("SHA-256").digest(password), passwordSha256)) {
                int employeeId = resultSet.getInt("employee_id");
                KeyGenerator kg = KeyGenerator.getInstance("HmacSHA256");
                SecretKey sk = kg.generateKey();
                Base64.Encoder base64Encoder = Base64.getUrlEncoder();
                String token = base64Encoder.encodeToString(sk.getEncoded());
                String createEmployeeAuthorizationSessionSql =
                        "INSERT INTO employee_authorization_session (employee_id, token) VALUES (?, ?)";
                try(
                        PreparedStatement createManagerAuthorizationSessionPreparedStatement = connection.prepareStatement(createEmployeeAuthorizationSessionSql);
                ) {
                    createManagerAuthorizationSessionPreparedStatement.setInt(1, employeeId);
                    createManagerAuthorizationSessionPreparedStatement.setString(2, token);
                    createManagerAuthorizationSessionPreparedStatement.execute();
                }
                fineLogger.fine("Created token for email: " + email + " / employeeId: " + employeeId + ". Token: " + token);
                finerLogger.exiting(Authenticate.class.getName(), "authenticateEmployee");
                return new Authentication(token);
            } else {
                fineLogger.fine("Password mismatch for email: " + email);
                finerLogger.exiting(Authenticate.class.getName(), "authenticateEmployee");
                return new Authentication();
            }

        }
    }
}
