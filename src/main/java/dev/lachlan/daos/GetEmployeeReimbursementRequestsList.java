package dev.lachlan.daos;

import dev.lachlan.Log;
import org.postgresql.Driver;

import java.sql.*;
import java.text.DecimalFormat;

import static dev.lachlan.Credentials.*;

public class GetEmployeeReimbursementRequestsList implements Log {
    static String getEmployeeReimbursementRequestsList(int employeeId) throws SQLException {
        finerLogger.entering(GetReimbursementsList.class.getName(), "getEmployeeReimbursementRequestsList");

        String sql =
                "SELECT reimbursement_request_id, amount_centesimal_integer, title, status FROM reimbursement_request WHERE employee_id = ?";

        DriverManager.registerDriver(new Driver());
        try (
                Connection connection = DriverManager.getConnection(POSTGRES_URL, POSTGRES_USER, POSTGRES_PASSWORD);
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ) {
            preparedStatement.setInt(1, employeeId);

            ResultSet resultSet = preparedStatement.executeQuery();
            int count = 0;
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.append("[");

            while (true) {

                if (resultSet.next()) {
                    int amountCentesimalInteger = resultSet.getInt("amount_centesimal_integer");
                    DecimalFormat decimalFormat = new DecimalFormat("00");

                    stringBuilder.append("{\"id\":\"" + resultSet.getString("reimbursement_request_id") + "\",\"title\":\"" + resultSet.getString("title") + "\",\"status\":\"" + resultSet.getString("status") + "\",\"amount\":\"$" + (amountCentesimalInteger / 100) + "." + decimalFormat.format(amountCentesimalInteger % 100) + "\"}");
                    count++;

                    if (!resultSet.isLast()) {
                        stringBuilder.append(",");
                    }

                } else {
                    break;
                }

            }

            stringBuilder.append("]");

            String reimbursementList = stringBuilder.toString();

            fineLogger.fine("Retrieved " + count + " records for reimbursement requests list. Return String: " + reimbursementList);
            finerLogger.exiting(GetReimbursementsList.class.getName(), "getEmployeeReimbursementRequestsList");
            return reimbursementList;
        }
    }
}
