package dev.lachlan.daos;

enum AmountArrayEnum {
    wholeNumber (0),
    decimal (1);

    int index;

    AmountArrayEnum(int index) {
        this.index = index;
    }
}
