package dev.lachlan.daos;

import dev.lachlan.Log;
import org.postgresql.Driver;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.text.DecimalFormat;

import static dev.lachlan.Credentials.POSTGRES_PASSWORD;
import static dev.lachlan.Credentials.POSTGRES_USER;
import static dev.lachlan.Credentials.POSTGRES_URL;

public class GetReimbursementsList implements Log {
    static String getReimbursementsList() throws SQLException {
        finerLogger.entering(GetReimbursementsList.class.getName(), "getEmployeesList");

        String sql =
                "SELECT reimbursement_request_id, amount_centesimal_integer, title, status, manager.first_name AS manager_first_name, manager.last_name AS manager_last_name, employee.first_name AS employee_first_name, employee.last_name AS employee_last_name FROM reimbursement_request LEFT JOIN manager ON manager.manager_id = resolving_manager_id LEFT JOIN employee ON employee.employee_id = reimbursement_request.employee_id;";

        DriverManager.registerDriver(new Driver());
        try (
                Connection connection = DriverManager.getConnection(POSTGRES_URL, POSTGRES_USER, POSTGRES_PASSWORD);
                Statement statement = connection.createStatement();
        ) {
            ResultSet resultSet = statement.executeQuery(sql);
            int count = 0;
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.append("[");

            while (true) {

                if (resultSet.next()) {
                    int amountCentesimalInteger = resultSet.getInt("amount_centesimal_integer");
                    DecimalFormat decimalFormat = new DecimalFormat("00");

                    stringBuilder.append("{\"id\":\"" + resultSet.getString("reimbursement_request_id") + "\",\"employeeName\":\"" + resultSet.getString("employee_first_name") + " " + resultSet.getString("employee_last_name") + "\",\"title\":\"" + resultSet.getString("title") + "\",\"status\":\"" + resultSet.getString("status") + "\",\"amount\":\"$" + (amountCentesimalInteger / 100) + "." + decimalFormat.format(amountCentesimalInteger % 100) + "\",\"approvingManagerName\":\"" + resultSet.getString("manager_first_name") + " " + resultSet.getString("manager_last_name") + "\"}");
                    count++;

                    if (!resultSet.isLast()) {
                        stringBuilder.append(",");
                    }

                } else {
                    break;
                }

            }

            stringBuilder.append("]");

            String reimbursementList = stringBuilder.toString();

            fineLogger.fine("Retrieved " + count + " records for reimbursements list. Return String: " + reimbursementList);
            finerLogger.exiting(GetReimbursementsList.class.getName(), "getReimbursementsList");
            return reimbursementList;
        }
    }
}
