package dev.lachlan.daos;

import dev.lachlan.Service;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;

public abstract class DaoService extends Service {
    protected DaoService() {
        super();
    }

    public String getEmployeesList() throws SQLException, IOException {
        return dev.lachlan.daos.GetEmployeesList.getEmployeesList();
    }

    public Authentication authenticate(String email, byte[] password) throws SQLException, DatabaseIntegrityException, InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, InvalidKeySpecException, BadPaddingException, InvalidKeyException {
        return dev.lachlan.daos.Authenticate.authenticate(email, password);
    }

    public boolean authorize(String authorizationToken) throws SQLException {
        return dev.lachlan.daos.Authorize.authorize(authorizationToken);
    }

    public String getReimbursementsList() throws SQLException {
        return dev.lachlan.daos.GetReimbursementsList.getReimbursementsList();
    }

    public String approveReimbursementRequest(int reimbursementRequestId, int managerId) throws SQLException, DatabaseIntegrityException {
        return ApproveReimbursementRequest.approveReimbursementRequest(reimbursementRequestId, managerId);
    }

    public int getManagerId(String authorizationToken) throws SQLException {
        return dev.lachlan.daos.Authorize.getManagerId(authorizationToken);
    }

    public String denyReimbursementRequest(int reimbursementRequestId, int managerId) throws SQLException, DatabaseIntegrityException {
        return dev.lachlan.daos.DenyReimbursementRequest.denyReimbursementRequest(reimbursementRequestId, managerId);
    }

    public Authentication authenticateEmployee(String email, byte[] password) throws SQLException, DatabaseIntegrityException, InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, InvalidKeySpecException, BadPaddingException, InvalidKeyException {
        return dev.lachlan.daos.Authenticate.authenticateEmployee(email, password);
    }

    public boolean authorizeEmployee(String authorizationToken) throws SQLException {
        return dev.lachlan.daos.Authorize.authorizeEmployee(authorizationToken);
    }

    public int getEmployeeId(String authorizationToken) throws SQLException {
        return dev.lachlan.daos.Authorize.getEmployeeId(authorizationToken);
    }

    public String getEmployeeReimbursementRequestsList(int employeeId) throws SQLException {
        return dev.lachlan.daos.GetEmployeeReimbursementRequestsList.getEmployeeReimbursementRequestsList(employeeId);
    }

    public String getProfile(int employeeId) throws SQLException {
        return dev.lachlan.daos.GetProfile.getProfile(employeeId);
    }

    public boolean logoutEmployee(String authorizationToken) throws SQLException, DatabaseIntegrityException {
        return LogoutEmployee.logoutEmployee(authorizationToken);
    }

    public boolean addReimburseRequest(int employeeId, String title, String amount) throws SQLException, DatabaseIntegrityException {
        return dev.lachlan.daos.AddReimbursementRequest.addReimbursementRequest(employeeId, title, amount);
    }

    public boolean logout(String authorizationToken) throws SQLException, DatabaseIntegrityException {
        return Logout.logout(authorizationToken);
    }
}
