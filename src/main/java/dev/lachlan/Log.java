package dev.lachlan;

import dev.lachlan.daos.DatabaseIntegrityException;
import dev.lachlan.daos.DatabaseOperationException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.Handler;
import java.util.logging.FileHandler;

public interface Log {
    static LocalDateTime logFileDate = LocalDateTime.now();
    public static final String logFilenamePrefix = "project1-dev.lachlan-" + logFileDate.format(DateTimeFormatter.ofPattern("D-H-m-s-n")) + Math.random();
    public static Logger finerLogger = Logger.getLogger(logFilenamePrefix + "-finer");
    public static Logger fineLogger = Logger.getLogger(logFilenamePrefix + "-fine");

    default void configureFinerLogger() throws IOException {
        finerLogger.setLevel(Level.FINER);

        Handler finerFileHandler = new FileHandler("%t/" + logFilenamePrefix + "-finer.log");

        finerFileHandler.setLevel(Level.FINER);
        finerLogger.addHandler(finerFileHandler);
    }

    default void configureFineLogger() throws IOException {
        fineLogger.setLevel(Level.FINE);

        Handler fineFileHandler = new FileHandler("%t/" + logFilenamePrefix + "-fine.log");

        fineFileHandler.setLevel(Level.FINE);
        fineLogger.addHandler(fineFileHandler);
    }

    default void log(SQLException throwable) {
        finerLogger.entering(throwable.getClass().getName(), "log");
        finerLogger.throwing(throwable.getClass().getName(), "log", throwable);
        finerLogger.exiting(throwable.getClass().getName(), "log");
    }

    default void log(DatabaseIntegrityException throwable) {
        finerLogger.entering(throwable.getClass().getName(), "log");
        finerLogger.throwing(throwable.getClass().getName(), "log", throwable);
        finerLogger.exiting(throwable.getClass().getName(), "log");
    }

    default void log(InvalidAlgorithmParameterException throwable) {
        finerLogger.entering(throwable.getClass().getName(), "log");
        finerLogger.throwing(throwable.getClass().getName(), "log", throwable);
        finerLogger.exiting(throwable.getClass().getName(), "log");
    }

    default void log(NoSuchPaddingException throwable) {
        finerLogger.entering(throwable.getClass().getName(), "log");
        finerLogger.throwing(throwable.getClass().getName(), "log", throwable);
        finerLogger.exiting(throwable.getClass().getName(), "log");
    }

    default void log(IllegalBlockSizeException throwable) {
        finerLogger.entering(throwable.getClass().getName(), "log");
        finerLogger.throwing(throwable.getClass().getName(), "log", throwable);
        finerLogger.exiting(throwable.getClass().getName(), "log");
    }

    default void log(NoSuchAlgorithmException throwable) {
        finerLogger.entering(throwable.getClass().getName(), "log");
        finerLogger.throwing(throwable.getClass().getName(), "log", throwable);
        finerLogger.exiting(throwable.getClass().getName(), "log");
    }

    default void log(InvalidKeySpecException throwable) {
        finerLogger.entering(throwable.getClass().getName(), "log");
        finerLogger.throwing(throwable.getClass().getName(), "log", throwable);
        finerLogger.exiting(throwable.getClass().getName(), "log");
    }

    default void log(BadPaddingException throwable) {
        finerLogger.entering(throwable.getClass().getName(), "log");
        finerLogger.throwing(throwable.getClass().getName(), "log", throwable);
        finerLogger.exiting(throwable.getClass().getName(), "log");
    }

    default void log(InvalidKeyException throwable) {
        finerLogger.entering(throwable.getClass().getName(), "log");
        finerLogger.throwing(throwable.getClass().getName(), "log", throwable);
        finerLogger.exiting(throwable.getClass().getName(), "log");
    }

//    default void log(DatabaseOperationException throwable) {
//        finerLogger.entering(throwable.getClass().getName(), "log");
//        finerLogger.throwing(throwable.getClass().getName(), "log", throwable);
//        finerLogger.exiting(throwable.getClass().getName(), "log");
//    }
}
