package dev.lachlan;

public final class Credentials {
    public static final String POSTGRES_URL = System.getenv("POSTGRES_URL");
    public static final String POSTGRES_USER = System.getenv("POSTGRES_USER");
    public static final String POSTGRES_PASSWORD = System.getenv("POSTGRES_PASSWORD");
}
