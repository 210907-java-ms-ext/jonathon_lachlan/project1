package dev.lachlan.servlets;

import dev.lachlan.Log;
import dev.lachlan.daos.DaoService;
import dev.lachlan.daos.DatabaseIntegrityException;
import dev.lachlan.daos.DatabaseOperationException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DenyReimbursementRequestServlet
 */
@WebServlet("/manager/authorized/deny-reimbursement-request")
public final class DenyReimbursementRequestServlet extends HttpServlet implements Log {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DenyReimbursementRequestServlet() {
        super();
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		try {
			configureFinerLogger();
			configureFineLogger();
		} catch (IOException throwable) {
			throw new ServletException("IOException with servlet configuration");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		finerLogger.entering(DenyReimbursementRequestServlet.class.getName(), "doPost");

		StringBuilder fineLogMessage = new StringBuilder("HTTP request information:\r\nURL: " + request.getRequestURL().toString() + "\r\nQuery String: " + request.getQueryString() + "\r\nProtocol: " + request.getProtocol() + "\r\nHeaders:\r\n");
		Iterator headerNamesIterator = request.getHeaderNames().asIterator();

		while (headerNamesIterator.hasNext()) {
			String headerName = headerNamesIterator.next().toString();
			Iterator headerIterator = request.getHeaders(headerName).asIterator();

			while (headerIterator.hasNext()) {
				fineLogMessage.append("- " + headerName + ": " + headerIterator.next().toString() + "\r\n");
			}

		}

		fineLogger.fine(fineLogMessage.toString());

		String reimbursementRequestId = request.getParameter("reimbursementRequestId");
		String authorizationToken = request.getHeader("Authorization");
		DaoService daoService = new ServletDaoService();
		try {
			int managerId = daoService.getManagerId(authorizationToken);

			if(managerId >= 0) {
				String managerName = daoService.denyReimbursementRequest(Integer.parseInt(reimbursementRequestId), managerId);

				response.getWriter().append(managerName);
				response.setStatus(200);
				fineLogger.fine("Authorized authorization token: " + authorizationToken + ".");
			} else {
				response.setStatus(401);
				fineLogger.fine("Unauthorized response with authorization token: " + authorizationToken);
			}

//		} catch (DatabaseOperationException throwable) {
//			log(throwable);
		} catch (SQLException throwable) {
			log(throwable);
		} catch (DatabaseIntegrityException throwable) {
			log(throwable);
		}
	}

}
