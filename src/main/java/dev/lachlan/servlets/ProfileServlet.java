package dev.lachlan.servlets;

import dev.lachlan.Log;
import dev.lachlan.daos.DaoService;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;

/**
 * Servlet implementation class EmployeesListServlet
 */
@WebServlet("/employee/authorized/profile")
public final class ProfileServlet extends HttpServlet implements Log {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProfileServlet() throws IOException {
        super();
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		try {
			configureFinerLogger();
			configureFineLogger();
		} catch (IOException throwable) {
			throw new ServletException("IOException with servlet configuration");
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		finerLogger.entering(ProfileServlet.class.getName(), "doGet");

		StringBuilder fineLogMessage = new StringBuilder("HTTP request information:\r\nURL: " + request.getRequestURL().toString() + "\r\nQuery String: " + request.getQueryString() + "\r\nProtocol: " + request.getProtocol() + "\r\nHeaders:\r\n");
		Iterator headerNamesIterator = request.getHeaderNames().asIterator();

		while(headerNamesIterator.hasNext()) {
			String headerName = headerNamesIterator.next().toString();
			Iterator headerIterator = request.getHeaders(headerName).asIterator();

			while(headerIterator.hasNext()) {
				fineLogMessage.append("- " + headerName + ": " + headerIterator.next().toString() + "\r\n");
			}

		}

		fineLogger.fine(fineLogMessage.toString());

		DaoService daoService = new ServletDaoService();
		String authorizationToken = request.getHeader("Authorization");
		try {
			int employeeId = daoService.getEmployeeId(authorizationToken);

			if(employeeId >= 0) {
				String employeeList = new ServletDaoService().getProfile(employeeId);
				response.getWriter().append(employeeList);
				fineLogger.fine("Authorized authorization token: " + authorizationToken + ". Sending employee list: " + employeeList);
			} else {
				response.setStatus(401);
				fineLogger.fine("Unauthorized response with authorization token: " + authorizationToken);
			}

		} catch (SQLException throwable) {
			log(throwable);
		}
		finerLogger.exiting(ProfileServlet.class.getName(), "doGet");
	}
}
