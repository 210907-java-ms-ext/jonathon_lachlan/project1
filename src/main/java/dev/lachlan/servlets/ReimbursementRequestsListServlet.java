package dev.lachlan.servlets;

import dev.lachlan.Log;
import dev.lachlan.daos.DaoService;
import dev.lachlan.daos.DatabaseOperationException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ReimbursementsListServlet
 */
@WebServlet("/manager/authorized/reimbursement-requests-list")
public final class ReimbursementRequestsListServlet extends HttpServlet implements Log {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReimbursementRequestsListServlet() {
        super();
    }

	public void init(ServletConfig config) throws ServletException {
		try {
			configureFinerLogger();
			configureFineLogger();
		} catch (IOException throwable) {
			throw new ServletException("IOException with servlet configuration");
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		finerLogger.entering(ReimbursementRequestsListServlet.class.getName(), "doGet");

		StringBuilder fineLogMessage = new StringBuilder("HTTP request information:\r\nURL: " + request.getRequestURL().toString() + "\r\nQuery String: " + request.getQueryString() + "\r\nProtocol: " + request.getProtocol() + "\r\nHeaders:\r\n");
		Iterator headerNamesIterator = request.getHeaderNames().asIterator();

		while(headerNamesIterator.hasNext()) {
			String headerName = headerNamesIterator.next().toString();
			Iterator headerIterator = request.getHeaders(headerName).asIterator();

			while(headerIterator.hasNext()) {
				fineLogMessage.append("- " + headerName + ": " + headerIterator.next().toString() + "\r\n");
			}

		}

		fineLogger.fine(fineLogMessage.toString());

		DaoService daoService = new ServletDaoService();
		String authorizationToken = request.getHeader("Authorization");
		try {

			if(daoService.authorize(authorizationToken)) {
				String reimbursementsList = new ServletDaoService().getReimbursementsList();
				response.setStatus(200);
				response.getWriter().append(reimbursementsList);
				fineLogger.fine("Authorized authorization token: " + authorizationToken + ". Sending reimbursements list: " + reimbursementsList);
			} else {
				response.setStatus(401);
				fineLogger.fine("Unauthorized response with authorization token: " + authorizationToken);
			}

		}catch (SQLException throwable) {
			log(throwable);
		}
		finerLogger.exiting(ReimbursementRequestsListServlet.class.getName(), "doGet");
	}

}
