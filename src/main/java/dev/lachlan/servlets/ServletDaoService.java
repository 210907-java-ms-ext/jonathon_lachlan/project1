package dev.lachlan.servlets;

import dev.lachlan.Log;
import dev.lachlan.daos.DaoService;
import java.io.IOException;

final class ServletDaoService extends DaoService implements Log {
    ServletDaoService() throws IOException {
        super();
        finerLogger.entering(ServletDaoService.class.getName(), "servletDaoService");
        finerLogger.exiting(ServletDaoService.class.getName(), "servletDaoService");
    }
}
