package dev.lachlan.servlets;

import dev.lachlan.Log;
import dev.lachlan.daos.Authentication;
import dev.lachlan.daos.DatabaseIntegrityException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.Iterator;

/**
 * Servlet implementation class ManagerAuthorizationServlet
 */
@WebServlet("/employee/login")
public final class EmployeeLoginServlet extends HttpServlet implements Log {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public EmployeeLoginServlet() {
        super();
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		try {
			configureFinerLogger();
			configureFineLogger();
		} catch (IOException throwable) {
			throw new ServletException("IOException with servlet configuration");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		finerLogger.entering(EmployeeLoginServlet.class.getName(), "doPost");

		StringBuilder fineLogMessage = new StringBuilder("HTTP request information:\r\nURL: " + request.getRequestURL().toString() + "\r\nQuery String: " + request.getQueryString() + "\r\nProtocol: " + request.getProtocol() + "\r\nHeaders:\r\n");
		Iterator headerNamesIterator = request.getHeaderNames().asIterator();

		while(headerNamesIterator.hasNext()) {
			String headerName = headerNamesIterator.next().toString();
			Iterator headerIterator = request.getHeaders(headerName).asIterator();

			while(headerIterator.hasNext()) {
				fineLogMessage.append("- " + headerName + ": " + headerIterator.next().toString() + "\r\n");
			}

		}

		fineLogger.fine(fineLogMessage.toString());

		String email = request.getParameter("email");
		byte[] password = request.getParameter("password").getBytes(StandardCharsets.UTF_8);
		try {
			Authentication authentication = new ServletDaoService().authenticateEmployee(email, password);

			if(authentication.isUser) {
				response.setStatus(200);
				response.addHeader("Authorization", authentication.authorizationToken);
				fineLogger.fine("Sending login success with email: " + email + " / authorization code: " + authentication.authorizationToken);
			} else {
				response.setStatus(401);
				fineLogger.fine("Unauthorized response with email: " + email);
			}

		} catch (SQLException throwable) {
			log(throwable);
		} catch (DatabaseIntegrityException throwable) {
			log(throwable);
		} catch (InvalidAlgorithmParameterException throwable) {
			log(throwable);
		} catch (NoSuchPaddingException throwable) {
			log(throwable);
		} catch (IllegalBlockSizeException throwable) {
			log(throwable);
		} catch (NoSuchAlgorithmException throwable) {
			log(throwable);
		} catch (InvalidKeySpecException throwable) {
			log(throwable);
		} catch (BadPaddingException throwable) {
			log(throwable);
		} catch (InvalidKeyException throwable) {
			log(throwable);
		}
		finerLogger.exiting(EmployeeLoginServlet.class.getName(), "doPost");
	}
}
