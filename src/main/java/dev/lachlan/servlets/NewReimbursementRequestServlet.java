package dev.lachlan.servlets;

import dev.lachlan.Log;
import dev.lachlan.daos.DaoService;
import dev.lachlan.daos.DatabaseIntegrityException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class NewReimbursementRequestServlet
 */
@WebServlet("/employee/authorized/new-reimbursement-request")
public final class NewReimbursementRequestServlet extends HttpServlet implements Log {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewReimbursementRequestServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		try {
			configureFinerLogger();
			configureFineLogger();
		} catch (IOException throwable) {
			throw new ServletException("IOException with servlet configuration");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		finerLogger.entering(ReimbursementRequestsListServlet.class.getName(), "doGet");

		StringBuilder fineLogMessage = new StringBuilder("HTTP request information:\r\nURL: " + request.getRequestURL().toString() + "\r\nQuery String: " + request.getQueryString() + "\r\nProtocol: " + request.getProtocol() + "\r\nHeaders:\r\n");
		Iterator headerNamesIterator = request.getHeaderNames().asIterator();

		while (headerNamesIterator.hasNext()) {
			String headerName = headerNamesIterator.next().toString();
			Iterator headerIterator = request.getHeaders(headerName).asIterator();

			while (headerIterator.hasNext()) {
				fineLogMessage.append("- " + headerName + ": " + headerIterator.next().toString() + "\r\n");
			}

		}

		fineLogger.fine(fineLogMessage.toString());

		DaoService daoService = new ServletDaoService();
		String authorizationToken = request.getHeader("Authorization");
		String title = request.getParameter("title");
		String amount = request.getParameter("amount");
		try {
			int employeeId = daoService.getEmployeeId(authorizationToken);
			System.out.println(employeeId);
			if (employeeId >= 0) {

				if(daoService.addReimburseRequest(employeeId, title, amount)) {
					response.setStatus(201);
				} else {
					response.setStatus(200);
				}

			} else {
				response.setStatus(401);
				fineLogger.fine("Unauthorized response with authorization token: " + authorizationToken);
			}

		} catch (SQLException throwable) {
			log(throwable);
		} catch (DatabaseIntegrityException throwable) {
			log(throwable);
		}
	}
}
