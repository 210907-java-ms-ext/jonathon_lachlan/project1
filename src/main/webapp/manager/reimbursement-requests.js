const loadReimbursementsListXmlHttpRequest = new XMLHttpRequest();

loadReimbursementsListXmlHttpRequest.open("GET", "authorized/reimbursement-requests-list");

loadReimbursementsListXmlHttpRequest.onreadystatechange = function () {

    if(loadReimbursementsListXmlHttpRequest.readyState === 4) {

        if(loadReimbursementsListXmlHttpRequest.status === 200) {
            const json = JSON.parse(loadReimbursementsListXmlHttpRequest.response);
            const tbody = document.getElementById("reimbursement-requests-list-table").getElementsByTagName("tbody")[0];

            for(const reimbursement of json) {
                const tr = document.createElement("tr");
                tr.setAttribute("data-id", reimbursement.id);
                const employeeNameTd = document.createElement("td");
                employeeNameTd.innerText = reimbursement.employeeName;

                tr.appendChild(employeeNameTd);

                const titleTd = document.createElement("td");
                titleTd.innerText = reimbursement.title;

                tr.appendChild(titleTd);

                const amountTd = document.createElement("td");
                amountTd.innerText = reimbursement.amount;

                tr.appendChild(amountTd);

                const statusTd = document.createElement("td");
                statusTd.innerText = reimbursement.status === "Approved" ? reimbursement.status + " by " + reimbursement.approvingManagerName : reimbursement.status;
                const approveErrorDiv = document.createElement("div");
                approveErrorDiv.hidden = true;

                statusTd.appendChild(approveErrorDiv);

                if(reimbursement.status === "Pending") {
                    const approveButton = document.createElement("button");
                    approveButton.innerText = "Approve";
                    approveButton.addEventListener("click", function () {
                        const approveXmlHttpRequest = new XMLHttpRequest();

                        approveXmlHttpRequest.open("POST", "authorized/approve-reimbursement-request");

                        approveXmlHttpRequest.onreadystatechange = function () {

                            if(approveXmlHttpRequest.readyState === 4) {

                                if(approveXmlHttpRequest.status === 200) {
                                    const approvingManagerName = approveXmlHttpRequest.response;
                                    statusTd.innerText = "Approved by " + approvingManagerName;

                                    approveButton.remove();

                                    approveErrorDiv.hidden = true;
                                }

                            } else {
                                approveErrorDiv.innerText = "A problem occurred with the approval.";
                                approveErrorDiv.hidden = false;
                            }

                        };

                        approveXmlHttpRequest.setRequestHeader("Authorization", authorizationToken);
                        approveXmlHttpRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                        approveXmlHttpRequest.send(`reimbursementRequestId=${reimbursement.id}`);
                    });

                    const denyButton = document.createElement("button");
                    denyButton.innerText = "Deny";
                    denyButton.addEventListener("click", function () {
                        const approveXmlHttpRequest = new XMLHttpRequest();

                        approveXmlHttpRequest.open("POST", "authorized/deny-reimbursement-request");

                        approveXmlHttpRequest.onreadystatechange = function () {

                            if(approveXmlHttpRequest.readyState === 4) {

                                if(approveXmlHttpRequest.status === 200) {
                                    const approvingManagerName = approveXmlHttpRequest.response;
                                    statusTd.innerText = "Denied by " + approvingManagerName;

                                    denyButton.remove();

                                    approveErrorDiv.hidden = true;
                                }

                            } else {
                                approveErrorDiv.innerText = "A problem occurred with the denial.";
                                approveErrorDiv.hidden = false;
                            }

                        };

                        approveXmlHttpRequest.setRequestHeader("Authorization", authorizationToken);
                        approveXmlHttpRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                        approveXmlHttpRequest.send(`reimbursementRequestId=${reimbursement.id}`);
                    });

                    statusTd.appendChild(approveButton);
                    statusTd.appendChild(denyButton);


                }

                tr.appendChild(statusTd);
                tbody.appendChild(tr);
            }

            document.getElementById("reimbursement-requests-list-table-loading-message-div").hidden = true;
            document.getElementById("reimbursement-requests-list-table").hidden = false;
        } else if(loadReimbursementsListXmlHttpRequest.status === 401) {
            const reimbursementListTableErrorMessageDiv = document.getElementById("reimbursement-requests-list-table-error-message-div");
            reimbursementListTableErrorMessageDiv.innerText = "Unauthorized. Please log in.";
            document.getElementById("reimbursement-requests-list-table-loading-message-div").hidden = true;
            reimbursementListTableErrorMessageDiv.hidden = false;
        } else {
            const reimbursementListTableErrorMessageDiv = document.getElementById("reimbursement-requests-list-table-error-message-div");
            reimbursementListTableErrorMessageDiv.innerText = "A problem occurred with loading the reimbursement requests list.";
            document.getElementById("reimbursement-requests-list-table-loading-message-div").hidden = true;
            reimbursementListTableErrorMessageDiv.hidden = false;
        }

    }

};

loadReimbursementsListXmlHttpRequest.setRequestHeader("Authorization", authorizationToken);
loadReimbursementsListXmlHttpRequest.send();