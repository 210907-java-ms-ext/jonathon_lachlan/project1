const xmlHttpRequest = new XMLHttpRequest();

xmlHttpRequest.open("GET", "authorized/employees-list");

xmlHttpRequest.onreadystatechange = function () {

    if(xmlHttpRequest.readyState === 4) {

        if(xmlHttpRequest.status === 200) {
            const json = JSON.parse(xmlHttpRequest.response);
            const tbody = document.getElementById("employees-list-table").getElementsByTagName("tbody");

            for(const employee of json) {
                const tr = document.createElement("tr");
                const firstNameTd = document.createElement("td");
                firstNameTd.innerText = employee.firstName;

                tr.appendChild(firstNameTd);

                const lastNameTd = document.createElement("td");
                lastNameTd.innerText = employee.lastName;

                tr.appendChild(lastNameTd);
                tbody[0].appendChild(tr);
            }

            document.getElementById("employees-list-table-loading-message-div").hidden = true;
            document.getElementById("employees-list-table").hidden = false;
        } else if(xmlHttpRequest.status === 401) {
            const employeesListTableErrorMessageDiv = document.getElementById("employee-list-table-error-message-div");
            employeesListTableErrorMessageDiv.innerText = "Unauthorized. Please log in.";
            document.getElementById("employees-list-table-loading-message-div").hidden = true;
            employeesListTableErrorMessageDiv.hidden = false;
        } else {
            const employeesListTableErrorMessageDiv = document.getElementById("employee-list-table-error-message-div");
            employeesListTableErrorMessageDiv.innerText = "A problem occurred with loading the employees list.";
            document.getElementById("employees-list-table-loading-message-div").hidden = true;
            employeesListTableErrorMessageDiv.hidden = false;
        }

    }

}

xmlHttpRequest.setRequestHeader("Authorization", authorizationToken);
xmlHttpRequest.send();