const loginForm = document.getElementById("login-form");

loginForm.addEventListener("submit", function (event) {
    event.preventDefault();

    const errorMessageDiv = document.getElementById("error-message-div");
    errorMessageDiv.innerText = "";
    errorMessageDiv.hidden = true;
    xmlHttpRequest = new XMLHttpRequest();

    xmlHttpRequest.open("POST", "login");

    xmlHttpRequest.onreadystatechange = function () {

        if(xmlHttpRequest.readyState === 4) {

            if(xmlHttpRequest.status === 200) {
                const authorizationToken = xmlHttpRequest.getResponseHeader("Authorization");

                sessionStorage.setItem("authorizationToken", authorizationToken);
                window.location.assign("home.html");
            } else {
                const errorMessageDiv = document.getElementById("error-message-div");
                errorMessageDiv.innerText = "Login unsuccessful.";
                errorMessageDiv.hidden = false;
            }

        }

    }

    xmlHttpRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    const email = document.getElementById("email-input").value;
    const password = document.getElementById("password-input").value;

  	loginForm.reset();
    xmlHttpRequest.send(`email=${email}&password=${password}`);
});