document.getElementById("logout-button").addEventListener("click", function (event) {
    loginButtonErrorDiv = document.getElementById("logout-button-error-div");

    loginButtonErrorDiv.innerText = "";
    loginButtonErrorDiv.hidden = true;

    const xmlHttpRequest = new XMLHttpRequest();

    xmlHttpRequest.open("POST", "logout");

    xmlHttpRequest.onreadystatechange = function () {

        if(xmlHttpRequest.readyState === 4) {

            if(xmlHttpRequest.status === 200) {
                sessionStorage.removeItem("authorizationToken");
                document.location.assign("login.html");
            } else if(xmlHttpRequest.status === 401) {
                document.location.assign("login.html");
            } else {
                loginButtonErrorDiv.innerText = "A problem occurred when attempting to log out.";
                loginButtonErrorDiv.hidden = false;
            }

        }

    };

    xmlHttpRequest.setRequestHeader("Authorization", authorizationToken);
    xmlHttpRequest.send();
});