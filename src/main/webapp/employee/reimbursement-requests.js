const loadReimbursementsListXmlHttpRequest = new XMLHttpRequest();

loadReimbursementsListXmlHttpRequest.open("GET", "authorized/reimbursement-requests-list");

loadReimbursementsListXmlHttpRequest.onreadystatechange = function () {

    if(loadReimbursementsListXmlHttpRequest.readyState === 4) {

        if(loadReimbursementsListXmlHttpRequest.status === 200) {
            const json = JSON.parse(loadReimbursementsListXmlHttpRequest.response);
            const tbody = document.getElementById("reimbursement-requests-list-table").getElementsByTagName("tbody")[0];

            for(const reimbursement of json) {
                const tr = document.createElement("tr");
                tr.setAttribute("data-id", reimbursement.id);

                const titleTd = document.createElement("td");
                titleTd.innerText = reimbursement.title;

                tr.appendChild(titleTd);

                const amountTd = document.createElement("td");
                amountTd.innerText = reimbursement.amount;

                tr.appendChild(amountTd);

                const statusTd = document.createElement("td");
                statusTd.innerText = reimbursement.status;
                const approveErrorDiv = document.createElement("div");
                approveErrorDiv.hidden = true;

                statusTd.appendChild(approveErrorDiv);

                tr.appendChild(statusTd);
                tbody.appendChild(tr);
            }

            document.getElementById("reimbursement-requests-list-table-loading-message-div").hidden = true;
            document.getElementById("reimbursement-requests-list-table").hidden = false;
        } else if(loadReimbursementsListXmlHttpRequest.status === 401) {
            const reimbursementListTableErrorMessageDiv = document.getElementById("reimbursement-requests-list-table-error-message-div");
            reimbursementListTableErrorMessageDiv.innerText = "Unauthorized. Please log in.";
            document.getElementById("reimbursement-requests-list-table-loading-message-div").hidden = true;
            reimbursementListTableErrorMessageDiv.hidden = false;
        } else {
            const reimbursementListTableErrorMessageDiv = document.getElementById("reimbursement-requests-list-table-error-message-div");
            reimbursementListTableErrorMessageDiv.innerText = "A problem occurred with loading the reimbursement requests list.";
            document.getElementById("reimbursement-requests-list-table-loading-message-div").hidden = true;
            reimbursementListTableErrorMessageDiv.hidden = false;
        }

    }

};

loadReimbursementsListXmlHttpRequest.setRequestHeader("Authorization", authorizationToken);
loadReimbursementsListXmlHttpRequest.send();