const reimbursementRequestForm = document.getElementById("reimbursement-request-form");

reimbursementRequestForm.addEventListener("submit", function(event) {
    event.preventDefault();

    const reimbursementRequestFormMessageDiv = document.getElementById("reimbursement-request-form-message-div");

    reimbursementRequestFormMessageDiv.innerText = "";
    reimbursementRequestFormMessageDiv.hidden = true;

    const xmlHttpRequest = new XMLHttpRequest();

    xmlHttpRequest.open("POST", "authorized/new-reimbursement-request");

    xmlHttpRequest.onreadystatechange = function () {

        if(xmlHttpRequest.readyState === 4) {

            if(xmlHttpRequest.status === 201) {
                reimbursementRequestFormMessageDiv.innerText = "Reimbursement request submitted.";
                reimbursementRequestFormMessageDiv.hidden = false;

                reimbursementRequestForm.reset();
            } else if(xmlHttpRequest.status === 200) {
                reimbursementRequestFormMessageDiv.innerText = "Inputs were not valid.";
                reimbursementRequestFormMessageDiv.hidden = false;
            } else if(xmlHttpRequest.status === 401) {
                reimbursementRequestFormMessageDiv.innerText = "Unauthorized. Please log in.";
                reimbursementRequestFormMessageDiv.hidden = false;
            } else {
                reimbursementRequestFormMessageDiv.innerText = "There was a problem with submitting the reimbursement request.";
                reimbursementRequestFormMessageDiv.hidden = false;
            }

        }

    };

    xmlHttpRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    const title = document.getElementById("title-input").value;
    const amount = document.getElementById("amount-input").value;

    xmlHttpRequest.setRequestHeader("Authorization", authorizationToken);
    xmlHttpRequest.send(`title=${title}&amount=${amount}`);
});