CREATE TABLE employee (
	employee_id serial NOT NULL PRIMARY key,
	first_name text NOT NULL,
	last_name text NOT NULL
);
CREATE TABLE manager (
	manager_id serial NOT NULL PRIMARY KEY,
	email text NOT NULL,
	password_sha256 bytea NOT NULL,
	first_name text NOT NULL,
	last_name text NOT NULL
);
CREATE TABLE manager_authorization_session (
	token text NOT NULL PRIMARY KEY,
	manager_id int NOT NULL REFERENCES manager
);
CREATE TABLE reimbursement_request (
    reimbursement_request_id serial NOT NULL PRIMARY KEY,
    employee_id int NOT NULL REFERENCES employee,
    amount_centesimal_integer int NOT NULL,
    title text NOT NULL,
    status text NOT NULL,
    resolving_manager_id int NOT NULL REFERENCES manager(manager_id)
);

--drop table authorizationsession, interests, newsoutlet, userinterest, useroutlet, users