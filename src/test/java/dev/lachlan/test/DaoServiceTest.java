package dev.lachlan.test;

import dev.lachlan.daos.DaoService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import java.io.IOException;
import java.sql.SQLException;

public class DaoServiceTest {

    @Test
    @DisplayName("DaoService can be implemented from another package")
    void daoServiceTest() throws SQLException, IOException {
        class DaoServiceImpl extends DaoService {}
    }
}