package dev.lachlan;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CredentialsTest {
    @Test
    void credentialsTest() {
        assertTrue(!Credentials.POSTGRES_URL.isEmpty() && Credentials.POSTGRES_URL.equals(System.getenv("POSTGRES_URL")));
        assertTrue(!Credentials.POSTGRES_USER.isEmpty() && Credentials.POSTGRES_USER.equals(System.getenv("POSTGRES_USER")));
        assertTrue(!Credentials.POSTGRES_PASSWORD.isEmpty() && Credentials.POSTGRES_PASSWORD.equals(System.getenv("POSTGRES_PASSWORD")));
    }
}
