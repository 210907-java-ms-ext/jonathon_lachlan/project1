package dev.lachlan.daos;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AuthenticationTest {

    @Test
    @DisplayName("Authentication(String authorizationToken)")
    void testAuthenticationStringAuthorization() {
        String token = "test_token";
        Authentication authentication = new Authentication(token);

        assertEquals(true, authentication.isUser);
        assertEquals(token, authentication.authorizationToken);
    }

    @Test
    @DisplayName("Authentication()")
    void testAuthentication() {
        Authentication authentication = new Authentication();
        assertEquals(false, authentication.isUser);
        assertEquals(null, authentication.authorizationToken);
    }
}
