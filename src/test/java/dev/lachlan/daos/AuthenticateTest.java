package dev.lachlan.daos;

import dev.lachlan.Log;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Nested;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;

import static dev.lachlan.daos.Authenticate.authenticate;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AuthenticateTest implements Log {
    static private final String TEST_POSTGRES_URL = System.getenv("POSTGRES_URL"),
            TEST_POSTGRES_USER = System.getenv("POSTGRES_USER"),
            TEST_POSTGRES_PASSWORD = System.getenv("POSTGRES_PASSWORD");
    private Handler finerFileHandler,
            fineFileHandler;

    @BeforeEach
    private void beforeEach() throws SQLException, IOException {
        finerFileHandler = new FileHandler("%t/" + logFilenamePrefix + "-TEST-finer.log");
        fineFileHandler = new FileHandler("%t/" + logFilenamePrefix + "-TEST-fine.log");
        String setupSql =
                "TRUNCATE manager_authorization_session, manager CASCADE;";
        try (
                Connection connection = DriverManager.getConnection(TEST_POSTGRES_URL, TEST_POSTGRES_USER, TEST_POSTGRES_PASSWORD);
                Statement statement = connection.createStatement()
        ) {
            statement.execute(setupSql);
        }
    }

    @AfterEach
    private void afterEach() throws SQLException, IOException {
        finerLogger.removeHandler(finerFileHandler);
        finerFileHandler.close();
        fineLogger.removeHandler(fineFileHandler);
        fineFileHandler.close();
        Files.deleteIfExists(Path.of(System.getProperty("java.io.tmpdir") + logFilenamePrefix + "-TEST-finer.log"));
        Files.deleteIfExists(Path.of(System.getProperty("java.io.tmpdir") + logFilenamePrefix + "-TEST-fine.log"));

        String setupSql =
                "TRUNCATE manager_authorization_session, manager CASCADE;";
        try (
                Connection connection = DriverManager.getConnection(TEST_POSTGRES_URL, TEST_POSTGRES_USER, TEST_POSTGRES_PASSWORD);
                Statement statement = connection.createStatement()
        ) {
            statement.execute(setupSql);
        }
    }

    @Test
    void testAuthenticate() throws NoSuchAlgorithmException, SQLException, DatabaseIntegrityException {
        MessageDigest messageDigestSha256 = MessageDigest.getInstance("SHA-256");
        byte[] passwordBytes = "test_password".getBytes(StandardCharsets.UTF_8);
        byte[] passwordSha256Bytes = messageDigestSha256.digest(passwordBytes);
        String email = "test_email@dev.lachlan";
        Authentication failedAuthentication = authenticate(email, passwordSha256Bytes);

        assertFalse(failedAuthentication.isUser);
        assertEquals(null, failedAuthentication.authorizationToken);

        String setupSql =
                "INSERT INTO manager (email, password_sha256, first_name, last_name) VALUES (?, ?, ?, ?)";
        try (
                Connection connection = DriverManager.getConnection(TEST_POSTGRES_URL, TEST_POSTGRES_USER, TEST_POSTGRES_PASSWORD);
                PreparedStatement setupPreparedStatement = connection.prepareStatement(setupSql)
        ) {
            setupPreparedStatement.setString(1, email);
            setupPreparedStatement.setBytes(2, passwordSha256Bytes);
            setupPreparedStatement.setString(3, "A");
            setupPreparedStatement.setString(4, "B");
            setupPreparedStatement.execute();
        }
        Authentication successfulAuthentication = authenticate(email, passwordBytes);

        assertTrue(successfulAuthentication.isUser);
        assertTrue(successfulAuthentication.authorizationToken != null && !successfulAuthentication.authorizationToken.isEmpty());
    }

    @Nested
    @DisplayName("authenticate() logs entering, exiting and a message for the result")
    class AuthenticateLogsEnteringExitingAndAMessageForTheResult {
        private String[] removeEmptyElements(String[] array) {
            ArrayList arrayList = new ArrayList();
            int count = 0;

            for(int i = 0; i < array.length; i++) {

                if(array[i].length() != 0)
                    arrayList.add(array[i]);

            }

            String[] newArray = new String[arrayList.size()];

            arrayList.toArray(newArray);
            return newArray;
        }

        @Test
        @DisplayName("Successful authentication")
        void successfulAuthentication() throws NoSuchAlgorithmException, SQLException, DatabaseIntegrityException, IOException, ParseException {
            fineLogger.setLevel(Level.FINE);
            fineFileHandler.setLevel(Level.FINE);
            fineLogger.addHandler(fineFileHandler);
            finerLogger.setLevel(Level.FINER);
            finerFileHandler.setLevel(Level.FINER);
            finerLogger.addHandler(finerFileHandler);

            MessageDigest messageDigestSha256 = MessageDigest.getInstance("SHA-256");
            byte[] passwordBytes = "test_password".getBytes(StandardCharsets.UTF_8);
            byte[] passwordSha256Bytes = messageDigestSha256.digest(passwordBytes);
            String email = "test_email@dev.lachlan";
            int managerId;
            String setupSql =
                    "INSERT INTO manager (email, password_sha256, first_name, last_name) VALUES (?, ?, ?, ?) RETURNING manager_id;";
            try (
                    Connection connection = DriverManager.getConnection(TEST_POSTGRES_URL, TEST_POSTGRES_USER, TEST_POSTGRES_PASSWORD);
                    PreparedStatement setupPreparedStatement = connection.prepareStatement(setupSql)
            ) {
                setupPreparedStatement.setString(1, email);
                setupPreparedStatement.setBytes(2, passwordSha256Bytes);
                setupPreparedStatement.setString(3, "A");
                setupPreparedStatement.setString(4, "B");
                ResultSet resultSet = setupPreparedStatement.executeQuery();

                resultSet.next();

                managerId = resultSet.getInt("manager_id");
            }
            Authentication authentication = authenticate(email, passwordBytes);

            String[] splitFinerLogXmlOnRecordTag = removeEmptyElements(new String(Files.readAllBytes(Path.of(System.getProperty("java.io.tmpdir") + logFilenamePrefix + "-TEST-finer.log"))).split("(<record>\n)|(</record>\n)"));

            assertEquals(3, splitFinerLogXmlOnRecordTag.length);
            assertEquals(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                            "<!DOCTYPE log SYSTEM \"logger.dtd\">\n" +
                            "<log>\n",
                    splitFinerLogXmlOnRecordTag[0]);

            String dateString;
            String[] dateStringSplit;
            Integer previousRecordNanos = null;
            int currentRecordNanos;
            Date previousRecordDate = null,
                    currentRecordDate;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");

            for(int i = 1; i < splitFinerLogXmlOnRecordTag.length; i++) {
                dateString = splitFinerLogXmlOnRecordTag[i].split("(<date>)|(</date>)")[1];

                assertEquals(true, dateString.matches("^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{6,9}Z$"));

                dateStringSplit = dateString.split("(?<=\\.\\d{3})|Z");
                currentRecordDate = simpleDateFormat.parse(dateStringSplit[0] + "Z");

                assertEquals(currentRecordDate, new Date(Long.parseLong(splitFinerLogXmlOnRecordTag[i].split("(<millis>)|(</millis>)")[1])));

                currentRecordNanos = Integer.parseInt(splitFinerLogXmlOnRecordTag[i].split("(<nanos>)|(</nanos>)")[1]);

                if(i > 1) {

                    // Millisecond precision
                    assertEquals(true, previousRecordDate.before(currentRecordDate) || previousRecordDate.equals(currentRecordDate));

                    if(previousRecordDate.equals(currentRecordDate) && previousRecordNanos != null)
                        assertTrue(previousRecordNanos <= currentRecordNanos);

                }

                assertEquals(logFilenamePrefix + "-finer", splitFinerLogXmlOnRecordTag[i].split("(<logger>)|(</logger>)")[1]);
                assertEquals("FINER", splitFinerLogXmlOnRecordTag[i].split("(<level>)|(</level>)")[1]);

                previousRecordDate = currentRecordDate;
                previousRecordNanos = currentRecordNanos;
            }

            assertEquals("0", splitFinerLogXmlOnRecordTag[1].split("(<sequence>)|(</sequence>)")[1]);
            assertEquals("dev.lachlan.daos.Authenticate", splitFinerLogXmlOnRecordTag[1].split("(<class>)|(</class>)")[1]);
            assertEquals("authenticate", splitFinerLogXmlOnRecordTag[1].split("(<method>)|(</method>)")[1]);
            assertEquals("ENTRY", splitFinerLogXmlOnRecordTag[1].split("(<message>)|(</message>)")[1]);
            assertEquals("2", splitFinerLogXmlOnRecordTag[2].split("(<sequence>)|(</sequence>)")[1]);
            assertEquals("dev.lachlan.daos.Authenticate", splitFinerLogXmlOnRecordTag[2].split("(<class>)|(</class>)")[1]);
            assertEquals("authenticate", splitFinerLogXmlOnRecordTag[2].split("(<method>)|(</method>)")[1]);
            assertEquals("RETURN", splitFinerLogXmlOnRecordTag[2].split("(<message>)|(</message>)")[1]);

            String[] splitFineLogXmlOnRecordTag = removeEmptyElements(new String(Files.readAllBytes(Path.of(System.getProperty("java.io.tmpdir") + logFilenamePrefix + "-TEST-fine.log"))).split("(<record>\n)|(</record>\n)"));

            assertEquals(2, splitFineLogXmlOnRecordTag.length);
            assertEquals(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                            "<!DOCTYPE log SYSTEM \"logger.dtd\">\n" +
                            "<log>\n",
                    splitFineLogXmlOnRecordTag[0]);

            for(int i = 1; i < splitFineLogXmlOnRecordTag.length; i++) {
                dateString = splitFineLogXmlOnRecordTag[i].split("(<date>)|(</date>)")[1];

                assertEquals(true, dateString.matches("^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{6,9}Z$"));

                dateStringSplit = dateString.split("(?<=\\.\\d{3})|Z");
                currentRecordDate = simpleDateFormat.parse(dateStringSplit[0] + "Z");

                assertEquals(currentRecordDate, new Date(Long.parseLong(splitFineLogXmlOnRecordTag[i].split("(<millis>)|(</millis>)")[1])));

                currentRecordNanos = Integer.parseInt(splitFineLogXmlOnRecordTag[i].split("(<nanos>)|(</nanos>)")[1]);

                if(i > 1) {

                    // Millisecond precision
                    assertEquals(true, previousRecordDate.before(currentRecordDate) || previousRecordDate.equals(currentRecordDate));

                    if(previousRecordDate.equals(currentRecordDate) && previousRecordNanos != null)
                        assertTrue(previousRecordNanos <= currentRecordNanos);

                }

                assertEquals(logFilenamePrefix + "-fine", splitFineLogXmlOnRecordTag[i].split("(<logger>)|(</logger>)")[1]);
                assertEquals("FINE", splitFineLogXmlOnRecordTag[i].split("(<level>)|(</level>)")[1]);

                previousRecordDate = currentRecordDate;
                previousRecordNanos = currentRecordNanos;
            }

            assertEquals("1", splitFineLogXmlOnRecordTag[1].split("(<sequence>)|(</sequence>)")[1]);
            assertEquals("dev.lachlan.daos.Authenticate", splitFineLogXmlOnRecordTag[1].split("(<class>)|(</class>)")[1]);
            assertEquals("authenticate", splitFineLogXmlOnRecordTag[1].split("(<method>)|(</method>)")[1]);
            assertEquals("Created token for email: " + email + " / managerId: " + managerId + ". Token: " + authentication.authorizationToken, splitFineLogXmlOnRecordTag[1].split("(<message>)|(</message>)")[1]);
        }

        @Test
        @DisplayName("Email not in database")
        void emailNotInDatabase() throws NoSuchAlgorithmException, SQLException, DatabaseIntegrityException, IOException, ParseException {
            fineLogger.setLevel(Level.FINE);
            fineFileHandler.setLevel(Level.FINE);
            fineLogger.addHandler(fineFileHandler);
            finerLogger.setLevel(Level.FINER);
            finerFileHandler.setLevel(Level.FINER);
            finerLogger.addHandler(finerFileHandler);

            byte[] passwordBytes = "test_password".getBytes(StandardCharsets.UTF_8);
            String email = "test_email@dev.lachlan";

            authenticate(email, passwordBytes);

            String[] splitFinerLogXmlOnRecordTag = removeEmptyElements(new String(Files.readAllBytes(Path.of(System.getProperty("java.io.tmpdir") + logFilenamePrefix + "-TEST-finer.log"))).split("(<record>\n)|(</record>\n)"));

            assertEquals(3, splitFinerLogXmlOnRecordTag.length);
            assertEquals(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                            "<!DOCTYPE log SYSTEM \"logger.dtd\">\n" +
                            "<log>\n",
                    splitFinerLogXmlOnRecordTag[0]);

            String dateString;
            String[] dateStringSplit;
            Integer previousRecordNanos = null;
            int currentRecordNanos;
            Date previousRecordDate = null,
                    currentRecordDate;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");

            for(int i = 1; i < splitFinerLogXmlOnRecordTag.length; i++) {
                dateString = splitFinerLogXmlOnRecordTag[i].split("(<date>)|(</date>)")[1];

                assertEquals(true, dateString.matches("^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{6,9}Z$"));

                dateStringSplit = dateString.split("(?<=\\.\\d{3})|Z");
                currentRecordDate = simpleDateFormat.parse(dateStringSplit[0] + "Z");

                assertEquals(currentRecordDate, new Date(Long.parseLong(splitFinerLogXmlOnRecordTag[i].split("(<millis>)|(</millis>)")[1])));

                currentRecordNanos = Integer.parseInt(splitFinerLogXmlOnRecordTag[i].split("(<nanos>)|(</nanos>)")[1]);

                if(i > 1) {

                    // Millisecond precision
                    assertEquals(true, previousRecordDate.before(currentRecordDate) || previousRecordDate.equals(currentRecordDate));

                    if(previousRecordDate.equals(currentRecordDate) && previousRecordNanos != null)
                        assertTrue(previousRecordNanos <= currentRecordNanos);

                }

                assertEquals(logFilenamePrefix + "-finer", splitFinerLogXmlOnRecordTag[i].split("(<logger>)|(</logger>)")[1]);
                assertEquals("FINER", splitFinerLogXmlOnRecordTag[i].split("(<level>)|(</level>)")[1]);

                previousRecordDate = currentRecordDate;
                previousRecordNanos = currentRecordNanos;
            }

            assertEquals("0", splitFinerLogXmlOnRecordTag[1].split("(<sequence>)|(</sequence>)")[1]);
            assertEquals("dev.lachlan.daos.Authenticate", splitFinerLogXmlOnRecordTag[1].split("(<class>)|(</class>)")[1]);
            assertEquals("authenticate", splitFinerLogXmlOnRecordTag[1].split("(<method>)|(</method>)")[1]);
            assertEquals("ENTRY", splitFinerLogXmlOnRecordTag[1].split("(<message>)|(</message>)")[1]);
            assertEquals("2", splitFinerLogXmlOnRecordTag[2].split("(<sequence>)|(</sequence>)")[1]);
            assertEquals("dev.lachlan.daos.Authenticate", splitFinerLogXmlOnRecordTag[2].split("(<class>)|(</class>)")[1]);
            assertEquals("authenticate", splitFinerLogXmlOnRecordTag[2].split("(<method>)|(</method>)")[1]);
            assertEquals("RETURN", splitFinerLogXmlOnRecordTag[2].split("(<message>)|(</message>)")[1]);

            String[] splitFineLogXmlOnRecordTag = removeEmptyElements(new String(Files.readAllBytes(Path.of(System.getProperty("java.io.tmpdir") + logFilenamePrefix + "-TEST-fine.log"))).split("(<record>\n)|(</record>\n)"));

            assertEquals(2, splitFineLogXmlOnRecordTag.length);
            assertEquals(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                            "<!DOCTYPE log SYSTEM \"logger.dtd\">\n" +
                            "<log>\n",
                    splitFineLogXmlOnRecordTag[0]);

            for(int i = 1; i < splitFineLogXmlOnRecordTag.length; i++) {
                dateString = splitFineLogXmlOnRecordTag[i].split("(<date>)|(</date>)")[1];

                assertEquals(true, dateString.matches("^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{6,9}Z$"));

                dateStringSplit = dateString.split("(?<=\\.\\d{3})|Z");
                currentRecordDate = simpleDateFormat.parse(dateStringSplit[0] + "Z");

                assertEquals(currentRecordDate, new Date(Long.parseLong(splitFineLogXmlOnRecordTag[i].split("(<millis>)|(</millis>)")[1])));

                currentRecordNanos = Integer.parseInt(splitFineLogXmlOnRecordTag[i].split("(<nanos>)|(</nanos>)")[1]);

                if(i > 1) {

                    // Millisecond precision
                    assertEquals(true, previousRecordDate.before(currentRecordDate) || previousRecordDate.equals(currentRecordDate));

                    if(previousRecordDate.equals(currentRecordDate) && previousRecordNanos != null)
                        assertTrue(previousRecordNanos <= currentRecordNanos);

                }

                assertEquals(logFilenamePrefix + "-fine", splitFineLogXmlOnRecordTag[i].split("(<logger>)|(</logger>)")[1]);
                assertEquals("FINE", splitFineLogXmlOnRecordTag[i].split("(<level>)|(</level>)")[1]);

                previousRecordDate = currentRecordDate;
                previousRecordNanos = currentRecordNanos;
            }

            assertEquals("1", splitFineLogXmlOnRecordTag[1].split("(<sequence>)|(</sequence>)")[1]);
            assertEquals("dev.lachlan.daos.Authenticate", splitFineLogXmlOnRecordTag[1].split("(<class>)|(</class>)")[1]);
            assertEquals("authenticate", splitFineLogXmlOnRecordTag[1].split("(<method>)|(</method>)")[1]);
            assertEquals("Record not found for email: " + email, splitFineLogXmlOnRecordTag[1].split("(<message>)|(</message>)")[1]);
        }

        @Test
        @DisplayName("Password mismatch")
        void passwordMismatch() throws NoSuchAlgorithmException, SQLException, DatabaseIntegrityException, IOException, ParseException {
            fineLogger.setLevel(Level.FINE);
            fineFileHandler.setLevel(Level.FINE);
            fineLogger.addHandler(fineFileHandler);
            finerLogger.setLevel(Level.FINER);
            finerFileHandler.setLevel(Level.FINER);
            finerLogger.addHandler(finerFileHandler);

            MessageDigest messageDigestSha256 = MessageDigest.getInstance("SHA-256");
            byte[] passwordBytes = "test_password".getBytes(StandardCharsets.UTF_8);
            byte[] mismatchPasswordBytes = "test_mismatch_password".getBytes(StandardCharsets.UTF_8);
            byte[] passwordSha256Bytes = messageDigestSha256.digest(passwordBytes);
            String email = "test_email@dev.lachlan";
            int managerId;
            String setupSql =
                    "INSERT INTO manager (email, password_sha256, first_name, last_name) VALUES (?, ?, ?, ?) RETURNING manager_id;";
            try (
                    Connection connection = DriverManager.getConnection(TEST_POSTGRES_URL, TEST_POSTGRES_USER, TEST_POSTGRES_PASSWORD);
                    PreparedStatement setupPreparedStatement = connection.prepareStatement(setupSql)
            ) {
                setupPreparedStatement.setString(1, email);
                setupPreparedStatement.setBytes(2, passwordSha256Bytes);
                setupPreparedStatement.setString(3, "A");
                setupPreparedStatement.setString(4, "B");
                ResultSet resultSet = setupPreparedStatement.executeQuery();

                resultSet.next();

                managerId = resultSet.getInt("manager_id");
            }
            Authentication authentication = authenticate(email, mismatchPasswordBytes);

            String[] splitFinerLogXmlOnRecordTag = removeEmptyElements(new String(Files.readAllBytes(Path.of(System.getProperty("java.io.tmpdir") + logFilenamePrefix + "-TEST-finer.log"))).split("(<record>\n)|(</record>\n)"));

            assertEquals(3, splitFinerLogXmlOnRecordTag.length);
            assertEquals(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                            "<!DOCTYPE log SYSTEM \"logger.dtd\">\n" +
                            "<log>\n",
                    splitFinerLogXmlOnRecordTag[0]);

            String dateString;
            String[] dateStringSplit;
            Integer previousRecordNanos = null;
            int currentRecordNanos;
            Date previousRecordDate = null,
                    currentRecordDate;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");

            for(int i = 1; i < splitFinerLogXmlOnRecordTag.length; i++) {
                dateString = splitFinerLogXmlOnRecordTag[i].split("(<date>)|(</date>)")[1];

                assertEquals(true, dateString.matches("^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{6,9}Z$"));

                dateStringSplit = dateString.split("(?<=\\.\\d{3})|Z");
                currentRecordDate = simpleDateFormat.parse(dateStringSplit[0] + "Z");

                assertEquals(currentRecordDate, new Date(Long.parseLong(splitFinerLogXmlOnRecordTag[i].split("(<millis>)|(</millis>)")[1])));

                currentRecordNanos = Integer.parseInt(splitFinerLogXmlOnRecordTag[i].split("(<nanos>)|(</nanos>)")[1]);

                if(i > 1) {

                    // Millisecond precision
                    assertEquals(true, previousRecordDate.before(currentRecordDate) || previousRecordDate.equals(currentRecordDate));

                    if(previousRecordDate.equals(currentRecordDate) && previousRecordNanos != null)
                        assertTrue(previousRecordNanos <= currentRecordNanos);

                }

                assertEquals(logFilenamePrefix + "-finer", splitFinerLogXmlOnRecordTag[i].split("(<logger>)|(</logger>)")[1]);
                assertEquals("FINER", splitFinerLogXmlOnRecordTag[i].split("(<level>)|(</level>)")[1]);

                previousRecordDate = currentRecordDate;
                previousRecordNanos = currentRecordNanos;
            }

            assertEquals("0", splitFinerLogXmlOnRecordTag[1].split("(<sequence>)|(</sequence>)")[1]);
            assertEquals("dev.lachlan.daos.Authenticate", splitFinerLogXmlOnRecordTag[1].split("(<class>)|(</class>)")[1]);
            assertEquals("authenticate", splitFinerLogXmlOnRecordTag[1].split("(<method>)|(</method>)")[1]);
            assertEquals("ENTRY", splitFinerLogXmlOnRecordTag[1].split("(<message>)|(</message>)")[1]);
            assertEquals("2", splitFinerLogXmlOnRecordTag[2].split("(<sequence>)|(</sequence>)")[1]);
            assertEquals("dev.lachlan.daos.Authenticate", splitFinerLogXmlOnRecordTag[2].split("(<class>)|(</class>)")[1]);
            assertEquals("authenticate", splitFinerLogXmlOnRecordTag[2].split("(<method>)|(</method>)")[1]);
            assertEquals("RETURN", splitFinerLogXmlOnRecordTag[2].split("(<message>)|(</message>)")[1]);

            String[] splitFineLogXmlOnRecordTag = removeEmptyElements(new String(Files.readAllBytes(Path.of(System.getProperty("java.io.tmpdir") + logFilenamePrefix + "-TEST-fine.log"))).split("(<record>\n)|(</record>\n)"));

            assertEquals(2, splitFineLogXmlOnRecordTag.length);
            assertEquals(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                            "<!DOCTYPE log SYSTEM \"logger.dtd\">\n" +
                            "<log>\n",
                    splitFineLogXmlOnRecordTag[0]);

            for(int i = 1; i < splitFineLogXmlOnRecordTag.length; i++) {
                dateString = splitFineLogXmlOnRecordTag[i].split("(<date>)|(</date>)")[1];

                assertEquals(true, dateString.matches("^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{6,9}Z$"));

                dateStringSplit = dateString.split("(?<=\\.\\d{3})|Z");
                currentRecordDate = simpleDateFormat.parse(dateStringSplit[0] + "Z");

                assertEquals(currentRecordDate, new Date(Long.parseLong(splitFineLogXmlOnRecordTag[i].split("(<millis>)|(</millis>)")[1])));

                currentRecordNanos = Integer.parseInt(splitFineLogXmlOnRecordTag[i].split("(<nanos>)|(</nanos>)")[1]);

                if(i > 1) {

                    // Millisecond precision
                    assertEquals(true, previousRecordDate.before(currentRecordDate) || previousRecordDate.equals(currentRecordDate));

                    if(previousRecordDate.equals(currentRecordDate) && previousRecordNanos != null)
                        assertTrue(previousRecordNanos <= currentRecordNanos);

                }

                assertEquals(logFilenamePrefix + "-fine", splitFineLogXmlOnRecordTag[i].split("(<logger>)|(</logger>)")[1]);
                assertEquals("FINE", splitFineLogXmlOnRecordTag[i].split("(<level>)|(</level>)")[1]);

                previousRecordDate = currentRecordDate;
                previousRecordNanos = currentRecordNanos;
            }

            assertEquals("1", splitFineLogXmlOnRecordTag[1].split("(<sequence>)|(</sequence>)")[1]);
            assertEquals("dev.lachlan.daos.Authenticate", splitFineLogXmlOnRecordTag[1].split("(<class>)|(</class>)")[1]);
            assertEquals("authenticate", splitFineLogXmlOnRecordTag[1].split("(<method>)|(</method>)")[1]);
            assertEquals("Password mismatch for email: " + email, splitFineLogXmlOnRecordTag[1].split("(<message>)|(</message>)")[1]);
        }
    }
}