package dev.lachlan.daos;

import dev.lachlan.Log;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.FileHandler;
import java.util.logging.Handler;

public class AddReimbursementRequestTest implements Log {
    static private final String TEST_POSTGRES_URL = System.getenv("POSTGRES_URL"),
            TEST_POSTGRES_USER = System.getenv("POSTGRES_USER"),
            TEST_POSTGRES_PASSWORD = System.getenv("POSTGRES_PASSWORD");
    private Handler finerFileHandler,
            fineFileHandler;

    @BeforeEach
    private void beforeEach() throws SQLException, IOException {
        finerFileHandler = new FileHandler("%t/" + logFilenamePrefix + "-TEST-finer.log");
        fineFileHandler = new FileHandler("%t/" + logFilenamePrefix + "-TEST-fine.log");
        String setupSql =
                "TRUNCATE employee, reimbursement_request CASCADE;";
        try (
                Connection connection = DriverManager.getConnection(TEST_POSTGRES_URL, TEST_POSTGRES_USER, TEST_POSTGRES_PASSWORD);
                Statement statement = connection.createStatement()
        ) {
            statement.execute(setupSql);
        }
    }

    @AfterEach
    private void afterEach() throws SQLException, IOException {
        finerLogger.removeHandler(finerFileHandler);
        finerFileHandler.close();
        fineLogger.removeHandler(fineFileHandler);
        fineFileHandler.close();
        Files.deleteIfExists(Path.of(System.getProperty("java.io.tmpdir") + logFilenamePrefix + "-TEST-finer.log"));
        Files.deleteIfExists(Path.of(System.getProperty("java.io.tmpdir") + logFilenamePrefix + "-TEST-fine.log"));

        String setupSql =
                "TRUNCATE employee, reimbursement_request CASCADE;";
        try (
                Connection connection = DriverManager.getConnection(TEST_POSTGRES_URL, TEST_POSTGRES_USER, TEST_POSTGRES_PASSWORD);
                Statement statement = connection.createStatement()
        ) {
            statement.execute(setupSql);
        }
    }

    @Test
    private void testAddReimbursementRequest() {

    }
}
