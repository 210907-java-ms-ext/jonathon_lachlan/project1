package dev.lachlan.daos;

import dev.lachlan.Log;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.logging.FileHandler;
import java.util.logging.Handler;

import static dev.lachlan.daos.DenyReimbursementRequest.denyReimbursementRequest;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DenyReimbursementRequestTest implements Log {
    static private final String TEST_POSTGRES_URL = System.getenv("POSTGRES_URL"),
            TEST_POSTGRES_USER = System.getenv("POSTGRES_USER"),
            TEST_POSTGRES_PASSWORD = System.getenv("POSTGRES_PASSWORD");
    private Handler finerFileHandler,
            fineFileHandler;

    @BeforeEach
    private void beforeEach() throws SQLException, IOException {
        finerFileHandler = new FileHandler("%t/" + logFilenamePrefix + "-TEST-finer.log");
        fineFileHandler = new FileHandler("%t/" + logFilenamePrefix + "-TEST-fine.log");
        String setupSql =
                "TRUNCATE employee, reimbursement_request, manager CASCADE;";
        try (
                Connection connection = DriverManager.getConnection(TEST_POSTGRES_URL, TEST_POSTGRES_USER, TEST_POSTGRES_PASSWORD);
                Statement statement = connection.createStatement()
        ) {
            statement.execute(setupSql);
        }
    }

    @AfterEach
    private void afterEach() throws SQLException, IOException {
        finerLogger.removeHandler(finerFileHandler);
        finerFileHandler.close();
        fineLogger.removeHandler(fineFileHandler);
        fineFileHandler.close();
        Files.deleteIfExists(Path.of(System.getProperty("java.io.tmpdir") + logFilenamePrefix + "-TEST-finer.log"));
        Files.deleteIfExists(Path.of(System.getProperty("java.io.tmpdir") + logFilenamePrefix + "-TEST-fine.log"));

        String setupSql =
                "TRUNCATE employee, reimbursement_request, manager CASCADE;";
        try (
                Connection connection = DriverManager.getConnection(TEST_POSTGRES_URL, TEST_POSTGRES_USER, TEST_POSTGRES_PASSWORD);
                Statement statement = connection.createStatement()
        ) {
            statement.execute(setupSql);
        }
    }

    @Test
    void testApproveReimbursementRequest() throws NoSuchAlgorithmException, SQLException, DatabaseOperationException, DatabaseIntegrityException {
        String email = "test-email@dev.lachlan";
        MessageDigest messageDigestSha256 = MessageDigest.getInstance("SHA-256");
        byte[] passwordSha256Bytes = messageDigestSha256.digest("test_password".getBytes(StandardCharsets.UTF_8));
        int reimbursementRequestId,
                managerId,
                employeeId;
        String employeeSetupSql =
                "INSERT INTO employee (first_name, last_name) VALUES ('S', 'A') RETURNING employee_id;",
                managerSetupSql =
                        "INSERT INTO manager (email, password_sha256, first_name, last_name) VALUES (?, ?, 'A', 'B') RETURNING manager_id;",
                reimbursementRequestSetupSql =
                        "INSERT INTO reimbursement_request (title, status, amount_centesimal_integer, employee_id, resolving_manager_id) VALUES ('Test reimbursement', 'Pending', 25054, ?, ?) RETURNING reimbursement_request_id;";
        try(
                Connection connection = DriverManager.getConnection(TEST_POSTGRES_URL, TEST_POSTGRES_USER, TEST_POSTGRES_PASSWORD);
                Statement employeeSetupStatement = connection.createStatement();
                PreparedStatement managerSetupPreparedStatement = connection.prepareStatement(managerSetupSql);
                PreparedStatement reimbursementRequestSetupPreparedStatement = connection.prepareStatement(reimbursementRequestSetupSql)
        ) {
            ResultSet employeeSetupResultSet = employeeSetupStatement.executeQuery(employeeSetupSql);

            employeeSetupResultSet.next();

            employeeId = employeeSetupResultSet.getInt("employee_id");

            managerSetupPreparedStatement.setString(1, email);
            managerSetupPreparedStatement.setBytes(2, passwordSha256Bytes);

            ResultSet managerSetupResultSet = managerSetupPreparedStatement.executeQuery();

            managerSetupResultSet.next();

            managerId = managerSetupResultSet.getInt("manager_id");

            reimbursementRequestSetupPreparedStatement.setInt(1, employeeId);
            reimbursementRequestSetupPreparedStatement.setInt(2, managerId);

            ResultSet reimbursementRequestSetupResultSet = reimbursementRequestSetupPreparedStatement.executeQuery();

            reimbursementRequestSetupResultSet.next();

            reimbursementRequestId = reimbursementRequestSetupResultSet.getInt("reimbursement_request_id");
        }
        String managerName = denyReimbursementRequest(reimbursementRequestId, managerId);

        assertEquals("A B", managerName);
    }
}
